# desafio

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).


### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).

# Requisitos.
Você precisará fazer o seguinte:

- Uma aplicação utilizando Nuxt.Js (versão 2 ou 3, você escolhe)

- A aplicação irá consumir uma API, essa API deverá ter 2 níveis de informação:
     - Listar entidades
     - Detalhar entidade

- A aplicação deverá:

     - Conter uma página de introdução

     - Uma página de listagem

     - Uma página de detalhamento 


- A página de detalhamento deverá ser acessada ao clicar no item da lista

Você pode usar o framework de CSS que desejar (Bootstrap, TailwindCSS, etc)
Você escolhe a API, um exemplo é a seguinte API: https://pokeapi.co/

No caso do exemplo vc faria uma aplicação que listasse um determinado numero de pokemons, e ao clicar nele, carregaria os atributos básicos dele em outra página

O intuito disso aqui é avaliar se você consegue fazer integração com o backend (api), se consegue aplicar alguns conceitos de estrutura de dados, se consegue utilizar rotas (paginas diferentes) e se consegue construir componentes dentro de uma aplicação utilizando vue.js 
(nuxt é um framework de vue.js que utilizamos)

Eu acredito que você consiga entregar até dia 25 de fevereiro (5 dias úteis e uns quebrados)

Documentação do nuxt: https://nuxtjs.org/
Documentação do bootstrap, caso queira utilizar: https://getbootstrap.com/
Documentação do tailwind, caso queira utilizar: https://tailwindcss.com/docs/installation

Materiais de apoio:

https://axios.nuxtjs.org/

https://reactjsexample.com/pokepoke-a-frontend-application-that-connects-to-a-free-api-called-pokeapi/amp/